package com.LTI.project.assignment.parentClasses;


import java.util.Random;

import com.LTI.project.assignment.superclass.Account;

public class SavingsAccount extends Account{
	
	//Data Members for Savings Account
	private int  lockerID, pinNumber;
	String savingAccountNumber;
	double savingRate;
	String adharCardNumber, adharCardLastDigits, accountTypeName;


	public SavingsAccount(char accountType, String adharCardNumber, String accountHoldersName, double balanceAmmount) {
		super(accountType, accountHoldersName, balanceAmmount);
		this.adharCardNumber = adharCardNumber;
	}

	@Override
	public void savingRateCalculation() {

		savingRate=baseInterestRate-0.5;
		if(accountType=='1') {

			accountTypeName="Savings Account";
		}


	}

	@Override
	public String showDeatils(int i) {
		return "Saving Account ID  "+i+"\n"+" Saving Account Number=" + savingAccountNumber + "\n Locker ID=" + lockerID + "\n Saving Rate="
				+ savingRate + "\n Adhar Card Number=" + adharCardNumber + "\n Account Type=" + accountTypeName
				+ "\n Account Holders Name=" + accountHoldersName + "\n Balance Ammount=" + balanceAmmount;
	}

	public void pinNumber() {
		Random rand = new Random();
		String random4digitcode = Integer.toString(rand.nextInt(9000)+1000);
		lockerID=Integer.parseInt(random4digitcode);
	}

	public void lockerID() {
		Random rand = new Random();
		String random3digitcode = Integer.toString(rand.nextInt(900)+100);
		lockerID=Integer.parseInt(random3digitcode);
	}

	public void savingAccountNumber() {
		savingAccountNumber=Account.accountNumberCalculation(accountType,adharCardLastDigits );
	}

	public void adharcard() {
		adharCardLastDigits=adharCardNumber.substring(9, 12);

	}


	public String getSavingAccountNumber() {
		return savingAccountNumber;
	}


	public void setAccountType(char accountType) {
		this.accountType = accountType;
	}


	public void setAdharCardNumber(String adharCardNumber) {
		this.adharCardNumber = adharCardNumber;
	}









}
