package com.LTI.project.assignment.parentClasses;

import java.util.Random;

import com.LTI.project.assignment.superclass.Account;

public class CurrentAccount extends Account {


	private int pinNumber;
	String cardNumber,accountTypeName;
	String currentAccountNumber;
	double currentRate;

	String adharCardNumber, adharCardLastDigits;

	public CurrentAccount(char accountType, String adharCardNumber, String accountHoldersName, double balanceAmmount) {
		super(accountType,accountHoldersName, balanceAmmount);
		this.adharCardNumber = adharCardNumber;
	}

	public void pinNumber() {
		Random rand = new Random();
		String random4digitcode = Integer.toString(rand.nextInt(9000)+1000);
		pinNumber=Integer.parseInt(random4digitcode);
	}

	public void cardNumber() {
		Random rand = new Random();
		String random16digitcode = currentAccountNumber+Integer.toString(rand.nextInt(90000)+10000);
		cardNumber=random16digitcode;
	}

	public void currentAccountNumber() {
		currentAccountNumber=Account.accountNumberCalculation(accountType,adharCardLastDigits );
	}

	public void adharcard() {

		adharCardLastDigits=adharCardNumber.substring(9, 12);

	}

	public String getSavingAccountNumber() {
		return currentAccountNumber;
	}


	public void setAccountType(char accountType) {
		this.accountType = accountType;
	}


	public void setAdharCardNumber(String adharCardNumber) {
		this.adharCardNumber = adharCardNumber;
	}

	@Override
	public void currentRateCalculation() {

		currentRate=baseInterestRate*0.2;	
		if(accountType=='2') {

			accountTypeName="Current Account";
		}

	}

	@Override
	public String showDeatils(int i) {
		return "Current Account ID"+i+"\n Current Account Number =" + currentAccountNumber + "\n Card Number =" + cardNumber
				+ "\n Current Rate =" + currentRate + "\n Adhar Card Number =" + adharCardNumber + "\n Account Type ="
				+ accountTypeName + "\n Account Holders Name=" + accountHoldersName + "\n Balance Ammount=" + balanceAmmount;
	}



}
