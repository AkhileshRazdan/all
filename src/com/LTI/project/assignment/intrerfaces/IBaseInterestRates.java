package com.LTI.project.assignment.intrerfaces;

public interface IBaseInterestRates {

	public double baseInterestRate=8.0;

	double savingRate=0, currentRate=0;

	void currentRateCalculation();

	void savingRateCalculation();

}
