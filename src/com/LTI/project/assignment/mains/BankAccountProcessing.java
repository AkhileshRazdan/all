package com.LTI.project.assignment.mains;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

import com.LTI.project.assignment.parentClasses.CurrentAccount;
import com.LTI.project.assignment.parentClasses.SavingsAccount;

public class BankAccountProcessing {

	public static void main(String[] args) throws IOException {
		
		//Intial Data Member Declearation
		char accountType;
		String accountHoldersName;
		double balanceAmmount;
		String adharCardNumber;
		String fileNameDefined = "data1.csv";

		//statment to get the number line in csv file.
		int noOfLines = (int) Files.lines(Paths.get(new File("data1.csv").getPath())).count();
		
		//for traversing
		int rows = noOfLines;
		int columns = 4;

		SavingsAccount[] savingAccount=new SavingsAccount[rows];
		CurrentAccount[] currentAccount=new CurrentAccount[rows];

		int savingLoop=0,currentLoop=0;

		File file = new File(fileNameDefined);
		Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));

		System.out.println("==============================================================================");
		System.out.println("\t 	  Bank Account Creation Progarm");
		System.out.println("==============================================================================");
		
		//Data Holder
		String [][] myArray = new String[rows][columns];

		System.out.println("==============================================================================");
		System.out.println("\t 	  Total Number of records in csv file : "+myArray.length);
		System.out.println("==============================================================================");
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		//reading the data from CSV and storing as 2 D Array
		while((sc.hasNextLine())) {

			for (int i=0; i<myArray.length; i++) {

				String[] line = sc.nextLine().trim().split(",");

				for (int j=0; j<line.length; j++) {
					myArray[i][j] = line[j]; 
				}
			}
			//System.out.println(Arrays.deepToString(myArray));
		}
		
		/*while((data=sc.nextLine())!=null) {

			count++;


			for (int i=0; i<myArray.length; i++) {

				String[] line = data.trim().split(",");

				for (int j=0; j<line.length; j++) {
					myArray[i][j] = line[j]; 
				}
			}
			//System.out.println(Arrays.deepToString(myArray));
		}*/
		
		// System.out.println(Arrays.deepToString(myArray));
		sc.close();

		//LOGIC for main Program
		for (int i=0; i<myArray.length; i++) {
			
			//using Savings Acount
			if(myArray[i][0].equals("Saving")){

				accountType='1';

				accountHoldersName=myArray[i][2];
				balanceAmmount=Double.parseDouble(myArray[i][3]);
				adharCardNumber=myArray[i][1];

				savingAccount[savingLoop]=new SavingsAccount(accountType,adharCardNumber,accountHoldersName,balanceAmmount);

				savingAccount[savingLoop].adharcard();
				savingAccount[savingLoop].savingAccountNumber();
				savingAccount[savingLoop].lockerID();
				savingAccount[savingLoop].savingRateCalculation();
				savingAccount[savingLoop].pinNumber();
				savingLoop+=1;
			}
			// using Current Account
			else if((myArray[i][0].equals("Current"))){

				accountType='2';

				accountHoldersName=myArray[i][2];
				balanceAmmount=Double.parseDouble(myArray[i][3]);
				adharCardNumber=myArray[i][1];

				currentAccount[currentLoop]=new CurrentAccount(accountType,adharCardNumber,accountHoldersName,balanceAmmount);

				currentAccount[currentLoop].adharcard();
				currentAccount[currentLoop].currentAccountNumber();
				currentAccount[currentLoop].cardNumber();
				currentAccount[currentLoop].currentRateCalculation();
				currentAccount[currentLoop].pinNumber();

				currentLoop +=1;
			}
		}

		// printing all data for which data is Processed
		System.out.println("===================Savings Account Proccessed=================================");
		
		//Display The Savings Account Processed
		for (int i=0; i<savingLoop; i++) {


			System.out.println(savingAccount[i].showDeatils(i+1));
			System.out.println("==============================================================================");

		}
		System.out.println();
		System.out.println();
		System.out.println("==============================================================================");
		System.out.println("==============================================================================");
		System.out.println();
		System.out.println();
		System.out.println("===================Current Account Proccessed=================================");
		
		//Display The Current Account Processed
		for (int i=0; i<currentLoop; i++) {


			System.out.println(currentAccount[i].showDeatils(i+1));
			System.out.println("==============================================================================");

		}

	}

}


